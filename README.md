#Proof of concept data supplement using d3.js

__Notes:__ 

* Currently the updated data is simply randomly generated and generic. This could very easily be updated to take values fed to it by JSPlasma. 

* A working example is hosted [here.](http://www.uvm.edu/~nstrayer/donutChartMusings)

* Donut (or pie) charts are generally frowned upon in visualization circles as they are very hard to parse meaningful information from. Humans have a hard time pulling real insites out of curved areas. Just a note of caution.