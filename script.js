var width = 640,
    height = 888,
    radius = Math.min(width, height) / 2;

var color = d3.scale.ordinal()
    .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

var arc = d3.svg.arc()
    .outerRadius(radius - 10)
    .innerRadius(radius*0.3);

var pie = d3.layout.pie()
    .sort(null)
    .value(function(d) { return d.population; });

var svg = d3.select("#vis").append("svg")
    .attr("width", width)
    .attr("height", height - 80)
  .append("g")
    .attr("transform", "translate(" + width / 2 + "," + ((height / 2)+30) + ")");

d3.csv("data.csv", function(error, data) {

  data.forEach(function(d) {
    d.population = +d.population;
  });

  var g = svg.selectAll(".arc")
      .data(pie(data))
    .enter().append("g")
      .attr("class", "arc");

  g.append("path")
      .attr("d", arc)
      .style("fill", function(d,i) { return color(i); })
      .on("mouseover", function(d){
        d3.select(this)
        .style("fill", "red")
      })
      .on("mouseout", function(d,i){
        d3.select(this)
        .style("fill", color(i))
      })

});

d3.select("svg").append("text")
  .text("update")
  .attr("x", width/2)
  .attr("y",height/2 + 35)
  .attr("text-anchor", "middle")
  .attr("font-size", "25px")
  .attr("font-family", "optima")
  .attr("fill", "white")
  .on("click", function(){
    updateChart()
  })

function getRandom(){
  return Math.floor(Math.random() * 1000) + 1
}

function updateChart(){
  
  var newData = []

  for (var i=0; i < 7; i++ ){
    newData.push({"data":getRandom(), "label": "data source " + i})
  }

  var newPie = d3.layout.pie()
    .sort(null)
    .value(function(d) { return d.data });

  d3.selectAll("path")
    .data(newPie(newData))
    .transition()
    .duration(800)
    .ease("log")
    .attr("d", arc)

  var statisticData = [ "Statistic 1: " + getRandom(),
                        "Statistic 2: " + getRandom()/2]

  var statText = d3.select("svg").selectAll("text")
    .data(statisticData, function(d){ return String(d).slice(0,11)})

  statText
    .enter()
    .append("text")
    .text(function(d){return d})
    .attr("x", 10)
    .attr("y", function(d,i){return 50 + (40 * i)})
    .attr("text-anchor", "start")
    .attr("font-size", "25px")
    .attr("font-family", "optima")
    .attr("fill", "white")

  statText
    .text(function(d){return d})


}

